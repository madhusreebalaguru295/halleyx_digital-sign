<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
  <head>
    <title>Convert PDF to JPEG</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
   
    <style type="text/css">    
      img {border-width: 0}
      * {font-family:'Lucida Grande', sans-serif;}
    </style>
	        <!-- jQuery -->
        <script src='jquery-3.0.0.js' type='text/javascript'></script>

        <!-- jSignature -->
        <script src="jSignature-master/libs/jSignature.min.js"></script>
        <script src="jSignature-master/libs/modernizr.js"></script>         <style>
            #signature{
                width: 100%;
                height: auto;
                border: 1px solid black;
            }
        </style>
  </head>
  <body>
    
<?php
$message = "";
$display = "";
if($_FILES)
{    
    $output_dir = "uploads/";    
    ini_set("display_errors",1);
    if(isset($_FILES["myfile"]))
    {
        $RandomNum   = time();
        
        $ImageName      = str_replace(' ','-',strtolower($_FILES['myfile']['name']));
        $ImageType      = $_FILES['myfile']['type']; //"image/png", image/jpeg etc.
     
        $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
        $ImageExt       = str_replace('.','',$ImageExt);
        if($ImageExt != "pdf")
        {
            $message = "Invalid file format only <b>\"PDF\"</b> allowed.";
        }
        else
        {
            $ImageName      = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
            $NewImageName = 'pdffile.pdf';         
            move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir. $NewImageName);        
            $location= __DIR__;
            $name       = $output_dir. $NewImageName;
            $num = count_pages($name);
            $message = "PDF converted to JPEG sucessfully!!";
        }
    }
}
    function count_pages($pdfname) {
          $pdftext = file_get_contents($pdfname);
          $num = preg_match_all("/\/Page\W/", $pdftext, $dummy);
          return $num;
        }

?>
<iframe style='width:60%;height:500px;float:left;' src='uploads/pdffile.pdf'></iframe>
<div style='width:39%;float:left;'>
<p style="
    text-align: center;
    font-size: 16px;
    text-transform: uppercase;
">Signature here</p>
       <!-- Signature area -->
        <div id="signature"></div><br/>
        <form id='signform' action='signsave.php' method='post'>
        <textarea name='image' style='display:none;' id='output'></textarea>
		</form>
        <input type='button' id='click' value='Save' style="
    width: 20%;
    font-size: 15px;
    padding: 5px;
    text-align: center;
    margin: 0 40%;
">
        <!-- Preview image -->
        <img style='display:none;' src='' id='sign_prev' style='display: none;' />
</div>
        <!-- Script -->
	    <script>
            $(document).ready(function() {

                // Initialize jSignature
                var $sigdiv = $("#signature").jSignature({'UndoButton':true});

                $('#click').click(function(){
                    // Get response of type image
                    var data = $sigdiv.jSignature('getData', 'image');

                    // Storing in textarea
                    $('#output').val(data);
                    // Alter image source
                    $('#sign_prev').attr('src',"data:"+data);
                    $('#sign_prev').show();
                    $('#signform')[0].submit();
                });
            });
	    </script>
</body>
</html>